import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		
		System.out.println("Please, type the number of the desired shape: Options:\n  [1] Square\n  [2] Triangle\n  [3] Rectangle\n  [4] Circle");
		System.out.print("\nNumber: ");
		Scanner sh = new Scanner(System.in);
		
		int shape = sh.nextInt();
		
		if (shape == 1) {
			
			// Run square
			System.out.print("Please, input the length of the side: ");
			Scanner side = new Scanner(System.in);
			Double len = side.nextDouble();
			Square square = new Square();
			square.func(len);

		} else if (shape == 2){
			
			// Run triangle
			System.out.print("Please, input the height of the triangle: ");
			Scanner heightScanTriangle = new Scanner(System.in);
			Double heightTriangle = heightScanTriangle.nextDouble();
			
			System.out.print("Please, input the length of the base of the triangle: ");
			Scanner baseScanTriangle = new Scanner(System.in);
			Double baseTriangle = baseScanTriangle.nextDouble();
			
			Triangle triangle = new Triangle();
			triangle.func(heightTriangle, baseTriangle);
			
		} else if (shape == 3) {
			
			// Run rectangle
			System.out.print("Please, input the height of the rectangle: ");
			Scanner heightScanRectangle = new Scanner(System.in);
			Double height = heightScanRectangle.nextDouble();
			
			System.out.print("Please, input the width of the rectangle: ");
			Scanner baseScanRectangle = new Scanner(System.in);
			Double baseRectangle = baseScanRectangle.nextDouble();
			
			Rectangle rectangle = new Rectangle();
			rectangle.func(height, baseRectangle);

		} else if (shape == 4) {
			
			// Run circle
			System.out.print("Please, input the radius of the circle: ");
			Scanner radiusScanCircle = new Scanner(System.in);
			Double radius = radiusScanCircle.nextDouble();
			
			Circle circle = new Circle();
			circle.func(radius);
			
		} else {
			System.out.println("Input not recognized.");
		}
			
	}

}
